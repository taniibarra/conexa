import React from "react";
import "./Episodes.css";
const EpisodesFirstRow = ({ episodes, episodeData }) => {
  return (
    <>
      <p className="title_episode">Character #1 - Only Episodes</p>
      <div className="episodes_container">
        <div className="episodes_content">
          {episodes.map((index, item) => {
            return (
              <p key={item} className="name_episodes">
                {index}
              </p>
            );
          })}
        </div>
        <div>
          {episodeData.map((index, item) => {
            return (
              <p key={item} className="date_episodes">
                {index}
              </p>
            );
          })}
        </div>
      </div>
    </>
  );
};

export default EpisodesFirstRow;
