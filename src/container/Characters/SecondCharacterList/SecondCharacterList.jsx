import React from "react";
import SecondListCards from "../../../components/SecondListCards";

const SecondCharacterList = ({ characters, setNameEpisode }) => {
  return (
    <>
      <p
        style={{
          textAlign: "center",
          fontFamily: "Red Hat Mono",
          fontWeight: "bold",
        }}
      >
        Character #2
      </p>
      {characters.map((item, index) => {
        return (
          <SecondListCards
            key={index}
            item={item}
            setNameEpisode={setNameEpisode}
          />
        );
      })}
    </>
  );
};

export default SecondCharacterList;
