import React from "react";
import Cards from "../../../components/Cards";

const FirstCharacterList = ({ characters, setNameEpisode }) => {
  return (
    <>
      <p
        style={{
          textAlign: "center",
          fontFamily: "Red Hat Mono",
          fontWeight: "bold",
        }}
      >
        Character #1
      </p>
      {characters.map((item, index) => {
        return (
          <Cards key={index} item={item} setNameEpisode={setNameEpisode} />
        );
      })}
    </>
  );
};

export default FirstCharacterList;
