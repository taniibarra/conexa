import React, { useEffect, useState } from "react";
import Footer from "../../components/Footer";
import Pagination from "../../components/Pagination";
import FirstCharacterList from "../Characters/FirstCharacterList/FirstCharacterList";
import SecondCharacterList from "../Characters/SecondCharacterList/SecondCharacterList";
import "./Main.css";
import axios from "axios";

const Main = () => {
  const [characters, setCharacters] = useState([]);
  const [pagination, setPagination] = useState({});
  const [episodeName, setEpisodeName] = useState([]);
  const [episodeData, setEpisodeData] = useState([]);
  const [episodeNameSecond, setEpisodeNameSecond] = useState([]);
  const [episodeDataSecond, setEpisodeDataSecond] = useState([]);
  const [sharedEpisodesName, setSharedEpisodesName] = useState([]);
  const [sharedEpisodesDate, setSharedEpisodesDate] = useState([]);
  const [pageNumber, updatePageNumber] = useState(1);

  const [nameEpisode, setNameEpisode] = useState();
  const [nameEpisodeSecond, setNameEpisodeSecond] = useState();

  const api = `https://rickandmortyapi.com/api/character/?page=${pageNumber}`;

  // obtiene los personajes
  const fetchCharacters = () => {
    axios
      .get(`${api}`)
      .then((data) => {
        console.log(data);
        setCharacters(data.data.results);
        setPagination(data.data.info);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // funciones para la paginacion

  const onNext = (e) => {
    e.preventDefault();
    if (pageNumber <= pagination.pages) {
      updatePageNumber(pageNumber + 1);
    }
    //fetchCharacters(pageNumber);
  };

  const onPrev = (e) => {
    e.preventDefault();
    if (pageNumber > 1) {
      updatePageNumber(pageNumber - 1);
    }
    //fetchCharacters(pageNumber);
  };

  // itera sobre las url de los episodios obtenidos del primer personaje seleccionado

  async function fetchEpisodes(arr) {
    let nameEpArray = [];
    let dataEpArray = [];

    for (let i = 0; i <= arr?.length - 1; i++) {
      await axios
        .get(arr[i])
        .then((data) => {
          nameEpArray.push(data?.data?.name);
          dataEpArray.push(data?.data?.air_date);
        })
        .catch((error) => {
          console.log(error);
        });
    }
    setEpisodeName(nameEpArray);
    setEpisodeData(dataEpArray);
  }

  // itera sobre las url de los episodios obtenidos del segundo personaje seleccionado

  async function fetchEpisodesSecond(arr) {
    let nameEpArray = [];
    let dataEpArray = [];

    for (let i = 0; i <= arr?.length - 1; i++) {
      await axios
        .get(arr[i])
        .then((data) => {
          nameEpArray.push(data?.data?.name);
          dataEpArray.push(data?.data?.air_date);
        })
        .catch((error) => {
          console.log(error);
        });
    }
    setEpisodeNameSecond(nameEpArray);
    setEpisodeDataSecond(dataEpArray);
  }

  // compara los dos arrays de nombres de episodios y busca coincidencias

  useEffect(() => {
    let shared = [];
    for (let i = 0; i < episodeName?.length; i++) {
      for (let j = 0; j < episodeNameSecond?.length; j++) {
        if (episodeNameSecond[j] === episodeName[i]) {
          shared.push(episodeNameSecond[j]);
        }
      }
    }
    setSharedEpisodesName(shared);
  }, [episodeName, episodeNameSecond]);

  // compara los dos arrays de fechas de episodios y busca coincidencias

  useEffect(() => {
    let shared = [];
    for (let i = 0; i < episodeData?.length; i++) {
      for (let j = 0; j < episodeDataSecond?.length; j++) {
        if (episodeDataSecond[j] === episodeData[i]) {
          shared.push(episodeDataSecond[j]);
        }
      }
    }
    setSharedEpisodesDate(shared);
  }, [episodeData, episodeDataSecond]);

  useEffect(() => {
    fetchCharacters();
    fetchEpisodes(nameEpisode);
    fetchEpisodesSecond(nameEpisodeSecond);
  }, [nameEpisode, nameEpisodeSecond, api]);

  return (
    <>
      <Pagination
        next={pagination.next}
        prev={pagination.prev}
        onPrevious={onPrev}
        onNext={onNext}
        pageNumber={pageNumber}
      />
      <div className="main_container">
        <div className="character-1">
          <FirstCharacterList
            characters={characters}
            setNameEpisode={setNameEpisode}
            nameEpisode={nameEpisode}
          />
        </div>
        <div className="character-2">
          <SecondCharacterList
            characters={characters}
            setNameEpisode={setNameEpisodeSecond}
            nameEpisode={nameEpisode}
          />
        </div>
        <footer className="footer_container">
          <Footer
            episodeName={episodeName}
            episodeData={episodeData}
            sharedEpisodesName={sharedEpisodesName}
            sharedEpisodesDate={sharedEpisodesDate}
            episodeNameSecond={episodeNameSecond}
            episodeDataSecond={episodeDataSecond}
          />
        </footer>
      </div>
    </>
  );
};

export default Main;
