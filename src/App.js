import "./App.css";
import Main from "./container/Main/Main.jsx";

function App() {
  return (
    <div className="App">
      <Main />
    </div>
  );
}

export default App;
