import React from "react";
import "./Pagination.css";

const Pagination = ({ prev, next, onPrevious, onNext, pageNumber }) => {
  return (
    <>
      <img src="rickandmorty.png" alt="rick and morty logo" className="image" />
      <nav className="pagination_container">
        <div className="pagination">
          {prev ? (
            <button className="page-item" onClick={(e) => onPrevious(e)}>
              Previous
            </button>
          ) : null}
          {next ? (
            <button className="page-item" onClick={(e) => onNext(e)}>
              Next
            </button>
          ) : null}
        </div>
      </nav>
      <p className="pages">Page: {pageNumber}</p>
    </>
  );
};

export default Pagination;
