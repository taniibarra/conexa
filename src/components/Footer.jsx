import React from "react";
import EpisodesFirstRow from "../container/Episodes/EpisodesFirstRow";
import EpisodesLastRow from "../container/Episodes/EpisodesLastRow";
import SharedEpisodes from "../container/Episodes/SharedEpisodes";
import "../container/Main/Main.css";

const Footer = ({
  episodeName,
  episodeData,
  sharedEpisodesName,
  sharedEpisodesDate,
  episodeNameSecond,
  episodeDataSecond,
}) => {
  return (
    <>
      <div className="row-1">
        <EpisodesFirstRow episodes={episodeName} episodeData={episodeData} />
      </div>
      <div className="row-2">
        <SharedEpisodes
          episodes={sharedEpisodesName}
          episodeData={sharedEpisodesDate}
        />
      </div>
      <div className="row-3">
        <EpisodesLastRow
          episodes={episodeNameSecond}
          episodeData={episodeDataSecond}
        />
      </div>
    </>
  );
};

export default Footer;
