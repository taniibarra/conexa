import React from "react";
import "./Cards.css";

const Cards = ({ item, index, setNameEpisode }) => {
  const handleSelect = () => {
    setNameEpisode(item.episode);
  };

  return (
    <div className="card_container">
      <div className="card-1" key={index} onClick={handleSelect}>
        <img className="image" src={item.image} alt="image character" />
        <div className="card_content">
          <h2 className="name">{item.name}</h2>
          <p className="status-specie">
            {item.status} {item.species}
          </p>
        </div>
      </div>
    </div>
  );
};

export default Cards;
